#!/bin/bash
wget https://github.com/torakiki/sejda/releases/download/v3.2.85/sejda-console-3.2.85-bin.zip
unzip sejda-console-3.2.85-bin.zip
cd sejda-console-3.2.85/bin/ || exit

git clone https://git.kernel.org/pub/scm/docs/man-pages/man-pages
cd man-pages || exit
for i in man*/; do
    [ -e "$i" ] || break
    i=${i%/}
    cd "$i" || exit

    digit=${i##*n}

    for j in *."$digit"; do
        [ -e "$j" ] || break
        title="${j%.*}($digit)"
        man -t "./$j" | ps2pdf - "$title.pdf" &
    done
    wait

    for j in *.pdf; do
        title="${j%.*}"
        filename="$digit - ${j%(*}"
        exiftool -Title="$title" "$title.pdf"
        printf "BookmarkBegin\nBookmarkTitle: $title\nBookmarkLevel: 1\nBookmarkPageNumber: 1\n" "$title" >"./data"
        pdftk "$title.pdf" update_info "./data" output "$filename.pdf"
        mv "$filename.pdf" "../../"
    done

    title="man $digit"
    filename="${digit}000 - $title"
    echo "<br><br><center><h1>Section $digit</h1><br><h3>Linux manual pages</h3></center>" >"$title.html"
    wkhtmltopdf cover "$title.html" "$title.pdf"
    exiftool -Title="$title" "$title.pdf"
    printf "BookmarkBegin\nBookmarkTitle: $title\nBookmarkLevel: 1\nBookmarkPageNumber: 1\n" "$title" >"./data"
    pdftk "$title.pdf" update_info "./data" output "$filename.pdf"
    mv "$filename.pdf" "../../"
    cd ..
done

title="Linux manual pages"
filename="0000000 - $title"
today=$(date)
echo "<br><br><center><h1>Linux manual pages</h1><br><h3>Generated on $today</h3></center>" >"cover.html"
wkhtmltopdf cover "cover.html" "$title.pdf"
exiftool -Title="$title" "$title.pdf"
printf "BookmarkBegin\nBookmarkTitle: $title\nBookmarkLevel: 1\nBookmarkPageNumber: 1\n" "$title" >"./data"
pdftk "$title.pdf" update_info "./data" output "$filename.pdf"
mv "$filename.pdf" ..

cd ..
./sejda-console merge --overwrite -b retain -t doc_titles -f *.pdf -o man-pages.pdf
exiftool -Title="Linux manual pages" "man-pages.pdf"

mv man-pages.pdf ../../man-pages-sejda.pdf
cd ..
cd ..
rm -rf sejda-console-3.2.85
rm sejda-console-3.2.85-bin.zip
