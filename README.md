# Man Pages To PDF

Exports all man pages into one single PDF (with a table of contents)

## Download:
[Man-Pages.pdf](https://gitlab.com/quwepiro/man-pages-to-pdf/-/raw/main/man-pages.pdf)  
[Man-Pages-TOC.pdf](https://gitlab.com/quwepiro/man-pages-to-pdf/-/raw/main/man-pages-sejda.pdf)

## What you can do :)
Tell everyone this exists. I really wanted to have a pdf to add annotations and bookmarks to specific man pages and had to build everything from ground up.  
Please use your own marketing abilities :)

## How it works
The script clones all "official" man-pages from `https://git.kernel.org/pub/scm/docs/man-pages/man-pages` and converts them via `PostScript` into `PDF`.  
Once all man-pages were converted to `PDF` the java program `PdfMerger.ja` (credits go to: [Yanpas](https://github.com/Yanpas)) merges them into one big `PDF`.  
Because I do not know the exact syntax differences between `bash` and `sh` the `run.sh` only contains the following execution command: `bash man-pages-to-pdf.sh`

## Roadmap/ideas:
 - The cli (floss) tool `sejda console` (latest free of charge release is [v3.2.85](https://github.com/torakiki/sejda/releases/tag/v3.2.85)) has the feature to create a table of contents as a real first page in the pdf with clickable hyperlinks within the document, 
   but it is not possible to make a table of contents with first and second levels for each section
 - The use of a java program in a shell script is kinda hacky, it would be better to do all the table of contents handling directly in `pdftk`. However, this would need temporary files and some more knowledge about the exact format `pdftk` requires.
 - A stable production using Gitlab CI

## References/Credits:
 - https://github.com/Yanpas/PdfMerger  --> https://github.com/quwepiro/PdfMerger
 - https://github.com/wkhtmltopdf/wkhtmltopdf
 - https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/
 - https://blogs.gnome.org/happyaron/2010/09/13/convert-man-page-to-pdf/
 - https://github.com/torakiki/sejda/releases/tag/v3.2.85