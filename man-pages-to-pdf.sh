#!/bin/bash
git clone https://git.kernel.org/pub/scm/docs/man-pages/man-pages
cd man-pages || exit
for i in man*/; do
    [ -e "$i" ] || break
    i=${i%/}
    cd "$i" || exit

    digit=${i##*n}

    for j in *."$digit"; do
        [ -e "$j" ] || break
        name="${j%.*}($digit)"
        man -t "./$j" | ps2pdf - "$name.pdf" &
    done
    wait

    man="man $digit"

    echo "<br><br><center><h1>Section $digit</h1><br><h3>Linux manual pages</h3></center>" >"$man.html"
    wkhtmltopdf cover "$man.html" "$man.pdf"

    java -jar ../../PdfMerger.jar "$man.pdf" $(ls ./*.pdf | grep -v "$man.pdf") "$i-out.pdf"
    mv "$i-out.pdf" ../"$man.pdf"
    cd ..
done
wait

today=$(date)
echo "<br><br><center><h1>Linux manual pages</h1><br><h3>Generated on $today</h3></center>" >"cover.html"
wkhtmltopdf cover "cover.html" "cover.pdf"

pdftk "cover.pdf" [^"cover"]*.pdf cat output man-pages.pdf
exiftool -Title="Linux manual pages" "man-pages.pdf"
mv man-pages.pdf ..
cd ..
rm -rf man-pages
